import { Component } from '@angular/core';

@Component({
  selector: 'app-server-component',
  templateUrl: './server.component.html',
  styles: [
    `
      .online {
        color: white;
      }
    `
  ]
})

export class ServerComponent {
  serverid: number = 10;
  serverStatus: string = 'offline';

  constructor() {
    this.serverStatus = Math.random() > 0.5 ? 'online' : 'offline';
  }

  getServerState() {
    return this.serverStatus;
  }

  getColor() {
    return this.serverStatus === 'online' ? 'green' : 'red';
  }
}
